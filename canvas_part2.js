const canvas = document.getElementById("canpic");  // get canvas element from HTML by ID

const ctx = canvas.getContext("2d");


function drawTriangle(x, y, fillColor) {
  ctx.fillStyle = fillColor;

  ctx.beginPath();

  ctx.moveTo(x, y);

  ctx.lineTo(x + 100, y-100);

  ctx.lineTo(x + 200, y);

  ctx.fill();
}


var backgroundLinear = ctx.createLinearGradient(0, 0, 0, 350);

backgroundLinear.addColorStop(0, "#151c2e");

backgroundLinear.addColorStop(0.9, "white");

backgroundLinear.addColorStop(1,"green");

ctx.fillStyle = backgroundLinear;

ctx.fillRect(0, 0, 500, 450);

var gradTree = ctx.createLinearGradient(0, 2, 0, 260);

for (var n = 0; n < 100; n++) {

    var decimal_number = n * 0.01

    if (n%2 === 0) {
        gradTree.addColorStop(decimal_number, "#142f0e");
    } else {
        gradTree.addColorStop(decimal_number, "#327321");
    }
}

drawTriangle(30, 120, gradTree); //red triangle

drawTriangle(30, 190, gradTree); //blue triangle

drawTriangle(30, 260, gradTree); //green triangle


var gradTrunk = ctx.createLinearGradient(110, 0, 150, 0);

gradTrunk.addColorStop(0, "#22110c");
gradTrunk.addColorStop(0.5, "#582b1f");
gradTrunk.addColorStop(1, "#22110c");

ctx.fillStyle = gradTrunk;
ctx.fillRect(110, 260, 40, 100);


var xRed = 110
var xBlue = 160
var y = 75
var outerRadius = 20
var innerRadius = 1

for (var i = 0; i < 6; i++) {

   ctx.beginPath();

   if (i % 2 == 0) {

       var gradientRed = ctx.createRadialGradient(xRed, y, outerRadius, xRed + 10, y - 10, innerRadius);

       gradientRed.addColorStop(0, randomColor());
       gradientRed.addColorStop(1, 'white');

       ctx.fillStyle = gradientRed;
       ctx.arc(xRed, y, outerRadius, 0, 2 * Math.PI);
       ctx.fill();

   } else {

       var gradientBlue = ctx.createRadialGradient(xBlue, y, outerRadius, xBlue + 10, y - 10, innerRadius);

       gradientBlue.addColorStop(0, randomColor());
       gradientBlue.addColorStop(1, 'white');

       ctx.fillStyle = gradientBlue;
       ctx.arc(xBlue, y, outerRadius, 0, 2 * Math.PI);
       ctx.fill();

   }

   y = y + 30;

}


ctx.strokeStyle = 'white';

ctx.fillStyle = 'red';

ctx.lineWidth = 1;

ctx.font = '40px arial';

ctx.strokeText('Merry Christmas!', 30, 410);

ctx.fillText('Merry Christmas!', 30, 410);

var height_gift = 100
var width_gift = height_gift // you want square
var x_gift = 300
var y_gift = 260

for (var j = 0; j < 5; j++) {

   ctx.strokeStyle = "black";
   ctx.fillStyle = randomColor();

   ctx.fillRect(x_gift, y_gift, width_gift, height_gift);
   ctx.strokeRect(x_gift, y_gift, width_gift, height_gift);

   ctx.fillStyle = randomColor();

   var ribbon_width = 10 - j;

   // vertical ribbon
   ctx.fillRect(x_gift + width_gift/2 - ribbon_width/2 , y_gift, ribbon_width, height_gift);
   ctx.strokeRect(x_gift + width_gift/2 - ribbon_width/2 , y_gift, ribbon_width, height_gift);

   // horizontal ribbon
   ctx.fillRect(x_gift, y_gift + height_gift/2 - ribbon_width/2, width_gift, ribbon_width);
   ctx.strokeRect(x_gift, y_gift + height_gift/2 - ribbon_width/2, width_gift, ribbon_width);


   height_gift = ((4 - j) * 20)
   width_gift = height_gift
   x_gift = x_gift + 10
   y_gift = y_gift - height_gift

}

function randomInteger(max, min) {
   return Math.floor(Math.random()*(max-min)) + min;
}

function randomColor(){
   return 'rgb(' + randomInteger(0, 255) + ',' + randomInteger(0, 255) + ',' + randomInteger(0, 255) +')';
}

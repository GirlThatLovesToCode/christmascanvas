const canvas = document.getElementById("canpic");  // get canvas element from HTML by ID
const ctx = canvas.getContext("2d");

function drawTriangle(x, y) {
  ctx.fillStyle = "green";

  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.lineTo(x + 100, y-100);
  ctx.lineTo(x + 200, y);

  ctx.fill();
}

var backgroundLinear = ctx.createLinearGradient(0, 0, 0, 350);

backgroundLinear.addColorStop(0, "#151c2e");
backgroundLinear.addColorStop(0.9, "white");
backgroundLinear.addColorStop(1,"green");

ctx.fillStyle = backgroundLinear;

ctx.fillRect(0, 0, 500, 450);


drawTriangle(30, 120); //red triangle
drawTriangle(30, 190); //blue triangle
drawTriangle(30, 260); //green triangle

ctx.fillStyle = "brown";
ctx.fillRect(110, 260, 40, 100);

var y = 75;

for (var i = 0; i < 6; i++) {

   ctx.beginPath();

   if (i%2 == 0) {

       ctx.fillStyle = 'red';
       ctx.arc(110 , y, 20, 0, 2 * Math.PI);
       ctx.fill();

   } else {

       ctx.fillStyle = 'blue';
       ctx.arc(160, y, 20, 0, 2 * Math.PI);
       ctx.fill();

   }

y = y + 30;

}

ctx.strokeStyle = 'white';
ctx.fillStyle = 'red';
ctx.lineWidth = 1;
ctx.font = '40px arial';

ctx.strokeText('Merry Christmas!', 30, 410);
ctx.fillText('Merry Christmas!', 30, 410);